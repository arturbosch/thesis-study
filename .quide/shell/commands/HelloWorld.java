import io.gitlab.arturbosch.quide.shell.Command;

public class HelloWorld implements Command {

	public String getId() {
		return "hello";
	}
		
	public String run(String line) {
		return "Hello World!";
	}	
	
}