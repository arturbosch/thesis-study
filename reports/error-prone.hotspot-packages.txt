core/src/main/java/com/google/errorprone/scanner,252
check_api/src/main/java/com/google/errorprone/scanner,250
core/src/main/java/com/google/errorprone/bugpatterns/threadsafety,249
core/src/test/java/com/google/errorprone/matchers,163
test_helpers/src/main/java/com/google/errorprone,151
check_api/src/main/java/com/google/errorprone/util,145
check_api/src/main/java/com/google/errorprone/fixes,139
core/src/test/java/com/google/errorprone/bugpatterns/threadsafety,136
core/src/test/java/com/google/errorprone/bugpatterns/inject/testdata,118
core/src/main/java/com/google/errorprone/bugpatterns/inject/guice,103
core/src/test/java/com/google/errorprone/bugpatterns/inject/guice/testdata,67
check_api/src/main/java/com/google/errorprone/apply,60
check_api/src/main/java/com/google/errorprone/matchers/method,59
annotation/src/main/java/com/google/errorprone,59
check_api/src/main/java/com/google/errorprone/dataflow/nullnesspropagation,58
check_api/src/main/java/com/google/errorprone/bugpatterns,55
core/src/test/java/com/google/errorprone/bugpatterns/collectionincompatibletype/testdata,43
docgen_processor/src/main/java/com/google/errorprone,41
core/src/test/java/com/google/errorprone/refaster/testdata/template,39
core/src/test/java/com/google/errorprone/testdata,37
core/src/main/java/com/google/errorprone/bugpatterns/argumentselectiondefects,36
core/src/main/java/com/google/errorprone/bugpatterns/inject/dagger,32
test_helpers/src/test/java/com/google/errorprone,25
check_api/src/main/java/com/google/errorprone/suppliers,24
core/src/main/java/com/google/errorprone/refaster/annotation,24
core/src/main/java/com/google/errorprone/bugpatterns/android,22
check_api/src/test/java/com/google/errorprone/fixes,21
check_api/src/test/java/com/google/errorprone/matchers,20
docgen/src/test/java/com/google/errorprone/testdata,19
core/src/main/java/com/google/errorprone/bugpatterns/collectionincompatibletype,19
core/src/test/java/com/google/errorprone/bugpatterns/argumentselectiondefects,18
core/src/main/java/com/google/errorprone/bugpatterns/formatstring,18
core/src/test/java/com/google/errorprone/scanner,16
ant/src/main/java/com/google/errorprone,16
core/src/test/java/com/google/errorprone/fixes,15
check_api/src/main/java/com/google/errorprone/predicates/type,15
check_api/src/test/java/com/google/errorprone/apply,13
core/src/test/java/com/google/errorprone/util,12
core/src/test/java/com/google/errorprone/bugpatterns/testdata/proto,11
check_api/src/main/java/com/google/errorprone/names,10
docgen/src/main/java/com/google/errorprone/resources,8
check_api/src/test/java/com/google/errorprone/dataflow/nullnesspropagation,7
core/src/test/java/com/google/errorprone/bugpatterns/formatstring,7
core/src/test/java/com/google/errorprone/bugpatterns/nullness,6
annotation/src/test/java/com/google/errorprone,6
examples/plugin/bazel/java/com/google/errorprone/sample,5
check_api/src/test/java/com/google/errorprone/names,5
refaster/src/main/java/com/google/errorprone/refaster,5
annotations/src/main/java/com/google/errorprone/annotations/concurrent,5
examples/ant/ant_fork/src,5
core/src/main/java/com/google/errorprone/internal,4
core/src/main/java/com/google/errorprone/bugpatterns/nullness,4
examples/plugin/maven/sample_plugin/src/main/java/com/google/errorprone/sample,3
examples/plugin/gradle/sample_plugin/src/main/java/com/google/errorprone/sample,3
examples/gradle/src/main/java,2
examples/maven/error_prone_should_flag/src/main/java,2
examples/maven/annotation_processing_bug_repro/src/main/java,1
core/src/test/java/com/google/errorprone/refaster/testdata/input,1
core/src/main/java/com/google/errorprone/matchers,1
docs/bugpattern/argumentselectiondefects,1
core/src/main/java/com/google/errorprone/refactors/covariant_equals,1
core/src/test/java/com/google/errorprone/bugpatterns/inject/dagger/testdata/stubs/android/app,1
core/src/test/java/com/google/errorprone/refaster/testdata/output,1
core/src/test/java/com/google/errorprone/bugpatterns/inject/dagger/testdata/stubs/android/os,0
core/src/test/java/com/google/errorprone/bugpatterns/overloading/testdata,0
core/src/test/java/com/google/errorprone/bugpatterns/android/testdata/stubs/android/os,0
docs/bugpattern/android,0
examples/maven/refaster-based-cleanup/src/main/java,0
core/src/test/java/com/google/errorprone/bugpatterns/inject/dagger/testdata/stubs/android/content,0
core/src/test/java/com/google/errorprone/bugpatterns/android/testdata/stubs/android/graphics,0
docs/bugpattern/overloading,0
examples/plugin/maven/hello/src/main/java/com/google/errorprone/sample,0
examples/plugin/bazel/third_party/java/auto_service,0
core/src/main/java/com/google/errorprone/bugpatterns/overloading,0
core/src/test/java/com/google/errorprone/bugpatterns/android/testdata/stubs/android/util,0
examples/ant/compilerclasspath/src,0
core/src/test/java/com/google/errorprone/bugpatterns/android/testdata/stubs/android/preference,0
examples/ant/ant_lib/src,0
core/src/main/javatests/com/google/errorprone/bugpatterns,0
.idea/copyright,0
examples/refaster,0
core/src/test/java/com/google/errorprone/bugpatterns/android/testdata/stubs/android/support/v4/app,0
core/src/test/java/com/google/errorprone/bugpatterns/android/testdata/stubs/android/app,0
examples/plugin/gradle/hello/src/main/java/com/google/errorprone/sample,0