smells = ['LongMethod', 'FeatureEnvy', 'ComplexMethod',
          'NestedBlockDepth', 'BrainMethod', 'ComplexCondition',
          'DeadCode', 'Cycle', 'LongParameterList', 'GodClass',
          'LargeClass', 'MiddleMan', 'ShotgunSurgery',
          'ClassDataShouldBePrivate', 'TraditionBreaker',
          'DataClass', 'RefusedParentBequest']
distribution = [12, 1, 5, 3, 8, 7, 2, 15, 11, 10, 16, 14, 6, 13, 17, 4, 9]
survival = [10, 13, 9, 8, 12, 16, 15, 14, 17, 4, 5, 7, 1, 3, 11, 6, 2]
lifespan = [11, 15, 7, 6, 9, 16, 14, 10, 17, 5, 4, 12, 1, 2, 13, 8, 3]
mod_distribution = [8, 3, 10, 7, 6, 15, 14, 17, 16, 4, 12, 11, 5, 2, 1, 13, 9]
modifications = [9, 10, 7, 8, 5, 15, 16, 17, 4, 3, 1, 14, 2, 13, 11, 12, 6]
consistency = [5, 12, 8, 10, 9, 13, 15, 2, 17, 3, 1, 16, 7, 4, 11, 14, 6]
clusters = [17, 1, 4, 2, 17, 17, 3, 17, 17, 6, 17, 17, 5, 17, 17, 6, 6]


# cluster rankings
# FE:12
# NBD:6
# DC:5
# CM:4
# SS:3
# GC:2 DC:2 RPB:2
# LM:0 BM:0 CC:0 C:0 LPL:0 LC:0 MM:0 CDSBP:0 TB:0


def is_unique(a_list):
    if len(a_list) != len(set(a_list)):
        raise ValueError


is_unique(distribution)
is_unique(survival)
is_unique(lifespan)
is_unique(mod_distribution)
is_unique(modifications)
is_unique(consistency)

smell_to_rank = {}
for index, elem in enumerate(smells):
    rank = distribution[index] + survival[index] + lifespan[index] \
           + mod_distribution[index] + modifications[index] \
           + consistency[index] + clusters[index]
    rank = rank / 7
    smell_to_rank[elem] = rank
    print(elem, round(rank, 2),
          distribution[index], survival[index], lifespan[index],
          mod_distribution[index], modifications[index],
          consistency[index], clusters[index])

sorted_ranks = sorted(smell_to_rank.items(), key=lambda x: x[1])

print()
for key, value in sorted_ranks:
    print(key, '&', round(value, 2), '\\\\\\hline')
