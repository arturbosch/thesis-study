import os

projects_dir = "/home/artur/master/projects"

dirs = os.listdir(projects_dir)
for current in dirs:
    path = os.path.join(projects_dir, current, ".git")
    if not os.path.exists(path):
        print(path, "is no git repository")
        raise FileNotFoundError
print("Everything alright!")
