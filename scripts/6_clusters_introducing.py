import xml.etree.ElementTree as ElementTree
import os
from functools import reduce
import numpy
from io import StringIO

import pandas as pn
import matplotlib.pyplot as plt
from pylab import *
import seaborn as sb

root_path = '/home/artur/master/reports'


def extract_most_used_files():
    def get_files(start):
        for root, _, files in os.walk(start):
            for file in files:
                path = os.path.join(root, file)
                if path.endswith('.hotspot-files.txt'):
                    yield path

    def get_threshold(values):
        mean = numpy.mean(values)
        std = numpy.std(values)
        return round(mean + std * 2)

    def open_files():
        for file in get_files(root_path):
            with open(file) as current:
                yield current

    paths = []
    for file in open_files():
        pairs = {}
        for line in file.readlines():
            path, count = [x.strip() for x in line.split(',')][0:2]
            if path.endswith('.java') and 'test' not in path.lower():
                pairs[path] = int(count)
        threshold = get_threshold(list(pairs.values()))
        filtered = [key for key, value in pairs.items()
                    if value > threshold]
        paths += filtered
    return set(paths)


def get_xml_files(start):
    for root, _, files in os.walk(start):
        for file in files:
            path = os.path.join(root, file)
            if path.endswith('.xml'):
                yield path


introducing_table = {}  # type : int
all_introducing_table = {}  # type : int
consistent_table = {}  # type : int
all_consistent_table = {}  # type : int


def fill_with_zeros(dict):
    dict['LongMethod'] = 0
    dict['FeatureEnvy'] = 0
    dict['ComplexMethod'] = 0
    dict['NestedBlockDepth'] = 0
    dict['BrainMethod'] = 0
    dict['ComplexCondition'] = 0
    dict['DeadCode'] = 0
    dict['Cycle'] = 0
    dict['LongParameterList'] = 0
    dict['GodClass'] = 0
    dict['LargeClass'] = 0
    dict['MiddleMan'] = 0
    dict['ShotgunSurgery'] = 0
    dict['ClassDataShouldBePrivate'] = 0
    dict['TraditionBreaker'] = 0
    dict['DataClass'] = 0
    dict['RefusedParentBequest'] = 0


fill_with_zeros(introducing_table)
fill_with_zeros(all_introducing_table)
fill_with_zeros(consistent_table)
fill_with_zeros(all_consistent_table)


def extract_smell_sets_by_path(xml_file, most_used_files):
    print(xml_file)
    try:
        root = ElementTree.parse(xml_file).getroot()[0]  # root=Quide/Version
        result = {}
        for smell in root.findall('VersionedCodeSmell'):
            path = smell.get('path', None)
            if path in most_used_files:
                java_smell = smell.find('JavaCodeSmell')
                smell_type = java_smell.get('smellType', None)
                if path is not None and smell_type is not None:
                    if path in result.keys():
                        result[path].append(smell_type)
                    else:
                        result[path] = [smell_type]
                    handle_introducing_commits_and_consistency(smell,
                                                               smell_type)
        return map(lambda elem: set(elem), result.values())
    except ElementTree.ParseError:
        print("Invalid xml characters found.")
        return []


def handle_introducing_commits_and_consistency(smell, smell_type):
    introducing = smell.get('introducing', None)
    consistent = smell.get('consistent', None)
    if introducing is not None:
        if introducing == 'true':
            introducing_table[smell_type] = introducing_table[
                                                smell_type] + 1
            all_introducing_table[smell_type] = all_introducing_table[
                                                    smell_type] + 1
        else:
            all_introducing_table[smell_type] = all_introducing_table[
                                                    smell_type] + 1
    if consistent is not None:
        if consistent == 'true':
            consistent_table[smell_type] = consistent_table[
                                               smell_type] + 1
            all_consistent_table[smell_type] = all_consistent_table[
                                                   smell_type] + 1
        else:
            all_consistent_table[smell_type] = all_consistent_table[
                                                   smell_type] + 1


def print_as_latex_table_row(cluster, count):
    formatted = reduce(lambda elem, acc: acc + ', ' + elem, cluster, '')
    formatted = formatted.strip()[:-1]
    print(formatted, '&', count, '\\\\\\hline')


def add_to_cluster(sets_bigger_one, clusters):
    for smells in sets_bigger_one:
        key_as_set = tuple(sorted(smells))
        if key_as_set in clusters:
            clusters[key_as_set] += 1
        else:
            clusters[key_as_set] = 1


def main():
    clusters = {}
    most_used_files = extract_most_used_files()
    with open("7_all_clusters.csv", "a") as cluster_file:
        for xml_file in get_xml_files(root_path):
            smells_sets = extract_smell_sets_by_path(xml_file, most_used_files)
            for sset in smells_sets:
                cluster_file.write(" ".join(tuple(sorted(sset))) + "\n")
            sets_bigger_one = filter(lambda elem: len(elem) > 1, smells_sets)
            add_to_cluster(sets_bigger_one, clusters)

        for (cluster, count) in sorted(clusters.items(),
                                       key=lambda x: x[1],
                                       reverse=True):
            print_as_latex_table_row(cluster, count)


def consistency_and_introducing():
    intr_data = ['Typ,introducing-commit,non-introducing-commit']
    const_data = ['Typ,konsistent,nicht-konsistent']
    for key in introducing_table.keys():
        intr_value = introducing_table[key]
        intr_sum = all_introducing_table[key]
        intr_real = 100 / intr_sum * intr_value
        intr_not = 100 - intr_real
        intr_data.append(key + "," + str(intr_real) + "," + str(intr_not))
        const_value = consistent_table[key]
        const_sum = all_consistent_table[key]
        const_real = 100 / const_sum * const_value
        const_not = 100 - const_real
        const_data.append(key + "," + str(const_real) + "," + str(const_not))
    # intr_frame = pn.read_csv(StringIO("\n".join(intr_data))).sort_values(
    #     by='introducing-commit')
    # intr_frame[['introducing-commit']].plot.barh(intr_frame['Typ'])
    # const_frame = pn.read_csv(StringIO("\n".join(const_data))).sort_values(
    #     by='konsistent')
    # const_frame[['konsistent', 'nicht-konsistent']].plot.barh(
    #     const_frame['Typ'], stacked=True)
    # plt.show()
    # plt.savefig('6_introducing_commit.png', bbox_inches='tight')
    # plt.savefig('6_consistency.png', bbox_inches='tight')


if __name__ == '__main__':
    main()
    # consistency_and_introducing()
