import os


def read_data():
    # Header: "name,commits,sloc,#smells"
    reports = "/home/artur/master/reports"
    data_list = []
    file_list = os.listdir(reports)
    for file in file_list:
        if file.endswith("stats.txt"):
            path = os.path.join(reports, file)
            with open(path) as entry:
                data_list.append(entry.readline().strip())
    return data_list


class Entry:

    def __init__(self, name, commits, sloc, smells):
        self.smells = smells
        self.sloc = sloc
        self.commits = commits
        self.name = name

    def __str__(self) -> str:
        return "{} & {} & {} & {} \\\\\\hline" \
            .format(self.name, self.commits, self.sloc, self.smells)


def convert_data():
    entries = []
    for entry in data:
        name, commits, sloc, smells = entry.split(',')
        entries.append(Entry(name, commits, sloc, smells))
    return entries


if __name__ == '__main__':
    data = read_data()
    converted = convert_data()
    converted.sort(key=lambda x: x.name)
    for entry in converted:
        print(entry)
