import os
from functools import reduce
from io import StringIO

import pandas as pn
import matplotlib.pyplot as plt
from pylab import *
import seaborn as sb
import numpy as nmp
import string
from itertools import groupby
from scipy import stats
import dunn_test

type_lookup = {
    "FEATURE_ENVY": "FeatureEnvy",
    "COMPLEX_CONDITION": "ComplexCondition",
    "LONG_PARAM": "LongParameterList",
    "CYCLE": "Cycle",
    "LONG_METHOD": "LongMethod",
    "COMPLEX_METHOD": "ComplexMethod",
    "DATA_CLASS": "DataClass",
    "DEAD_CODE": "DeadCode",
    "NESTED_BLOCK_DEPTH": "NestedBlockDepth",
    "LARGE_CLASS": "LargeClass",
    "SHOTGUN_SURGERY": "ShotgunSurgery",
    "GOD_CLASS": "GodClass",
    "MIDDLE_MAN": "MiddleMan",
    "ClASS_DATA_SHOULD_BE_PRIVATE": "ClassDataShouldBePrivate",
    "BRAIN_METHOD": "BrainMethod",
    "TRADITION_BREAKER": "TraditionBreaker",
    "REFUSED_PARENT_BEQUEST": "RefusedParentBequest"
}


class DeadAliveRatio:

    def __init__(self, all, alive, dead):
        self.all = all
        self.alive = alive
        self.dead = dead

    def ratio(self):
        return self.alive / self.all * 100, self.dead / self.all * 100, self.dead / self.alive

    def __str__(self):
        palive, pdead, ratio = self.ratio()
        return "{} & {} & {} & {} & {} & {}" \
            .format(self.all, self.alive, self.dead, palive, pdead, ratio)


class SmellTypeData:

    def __init__(self, name, ratio, lifespan, changes, relocations, revivals):
        self.changes = changes
        self.relocations = relocations
        self.revivals = revivals
        self.lifespan = lifespan
        self.ratio = ratio
        self.name = name


class Lifespan:

    def __init__(self, entries):
        self.entries = entries


reports = "/home/artur/master/reports"

file_list = os.listdir(reports)

all_alive = 0
all_dead = 0
all_smells = 0

smell_type_data = []

# Daten einlesen

for file in file_list:
    if file.endswith("evaluation.txt"):
        path = os.path.join(reports, file)
        with open(path) as entry:
            for index, line in enumerate(entry.readlines()):
                if index == 0:
                    smells, alive, dead = map(lambda x: int(x),
                                              line.split(',')[0:3])
                    all_smells += smells
                    all_alive += alive
                    all_dead += dead
                else:
                    type, count, entries, versions, lifespan, changes, \
                    relocations, revivals = line.split('\t')[0:8]
                    palive, alive, dead = map(lambda x: int(x),
                                              entries.split(',')[0:3])
                    lifespan_entries = list(map(lambda x: float(x),
                                                lifespan.split(',')))
                    changes_entries = list(map(lambda x: float(x),
                                               changes.split(',')))
                    relocations_entries = list(map(lambda x: float(x),
                                                   relocations.split(',')))
                    revivals_entries = list(map(lambda x: float(x),
                                                revivals.split(',')))
                    smell_type_data.append(
                        SmellTypeData(type_lookup[type],
                                      DeadAliveRatio(palive, alive, dead),
                                      Lifespan(lifespan_entries),
                                      changes_entries, relocations_entries,
                                      revivals_entries))

data = DeadAliveRatio(all_smells, all_alive, all_dead)
print(data)


# # Dead Alive Ratio - Pie Chart
#
# palive, pdead, ratio = DeadAliveRatio(all_smells, all_alive, all_dead).ratio()
# print("all=", all_smells, "all_live=", all_alive, "all_dead=", all_dead,
#       "p_alive=", palive, "p_dead=", pdead, "dead/alive=", ratio)
#
# figure(1, figsize=(12, 12))
# ax = axes([0.1, 0.1, 0.8, 0.8])
#
# labels = 'Lebende', "Tote"
# fracs = [all_alive, all_dead]
# explode = (0.05, 0)
#
# pie(fracs, explode=explode, labels=labels,
#     autopct='%1.1f%%', shadow=True, startangle=90)
# plt.savefig('5_lebende_zu_tote_pie.png', bbox_inches='tight')
#
# # Dead Alive Ratio of Smell Types
#
# type_to_ratio = {}
# for data in smell_type_data:
#     type = data.name
#     if type not in type_to_ratio:
#         type_to_ratio[type] = data.ratio
#     else:
#         ratio = type_to_ratio[type]
#         nr = data.ratio
#         type_to_ratio[type] = DeadAliveRatio(ratio.all + nr.all,
#                                              ratio.alive + nr.alive,
#                                              ratio.dead + nr.dead)
#
# alive_data = ["name,lebendig,tot,ratio"]
# amount_data = ['name,smells']
# for key, value in type_to_ratio.items():
#     palive, pdead, ratio = [str(elem) for elem in value.ratio()]
#     alive_data.append(key + "," + palive + "," + pdead + "," + ratio)
#     amount_data.append(key + ',' + str(value.all))
#
# alive_frame = pn.read_csv(StringIO('\n'.join(alive_data))) \
#     .sort_values(by='lebendig')
#
# print(alive_frame)
#
# alive_frame[['lebendig', 'tot']].plot.barh(alive_frame['name'], stacked=True)
# plt.savefig('5_lebende_zu_tote_je_smell.png', bbox_inches='tight')
#
# amount_frame = pn.read_csv(StringIO('\n'.join(amount_data))) \
#     .sort_values(by='smells')
#
# print(amount_frame)
#
# amount_frame.plot.barh(amount_frame['name'], legend=False)
# plt.savefig('5_smell_type_verteilung.png', bbox_inches='tight')

# Lifespan

# lifespan_data = ['Typ,Lebensdauer']
# type_to_lifespan = {}
# for data in smell_type_data:
#     type = data.name
#     if type not in type_to_lifespan:
#         type_to_lifespan[type] = data.lifespan.entries
#     else:
#         current = type_to_lifespan[type]
#         new = data.lifespan
#         type_to_lifespan[type] = current + new.entries
#
# for key, value in type_to_lifespan.items():
#     for elem in value:
#         lifespan_data.append(key + "," + str(elem))
#
# lifespan_frame = pn.read_csv(StringIO('\n'.join(lifespan_data)))
# print(lifespan_frame)
#
#
# def kruskal(frame):
#     return stats.levene(
#         frame[lifespan_frame['Typ'] == 'LongMethod']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'FeatureEnvy']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'ComplexMethod']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'NestedBlockDepth']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'BrainMethod']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'ComplexCondition']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'DeadCode']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'Cycle']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'LongParameterList']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'GodClass']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'LargeClass']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'MiddleMan']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'ShotgunSurgery']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'ClassDataShouldBePrivate'][
#             'Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'TraditionBreaker']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'DataClass']['Lebensdauer'],
#         frame[lifespan_frame['Typ'] == 'RefusedParentBequest'][
#             'Lebensdauer']
#     )
#
#
# def column_for_each_smell(frame):
#     return pn.concat(pn.concat(
#         sub_frame_by_type(frame, 'LongMethod'),
#         sub_frame_by_type(frame, 'FeatureEnvy'),
#         sub_frame_by_type(frame, 'ComplexMethod'),
#         sub_frame_by_type(frame, 'NestedBlockDepth'),
#         sub_frame_by_type(frame, 'BrainMethod'),
#         sub_frame_by_type(frame, 'ComplexCondition'),
#         sub_frame_by_type(frame, 'DeadCode'),
#         sub_frame_by_type(frame, 'Cycle'),
#         sub_frame_by_type(frame, 'LongParameterList'),
#         sub_frame_by_type(frame, 'GodClass')
#     ),
#         sub_frame_by_type(frame, 'LargeClass'),
#         sub_frame_by_type(frame, 'MiddleMan'),
#         sub_frame_by_type(frame, 'ShotgunSurgery'),
#         sub_frame_by_type(frame, 'ClassDataShouldBePrivate'),
#         sub_frame_by_type(frame, 'TraditionBreaker'),
#         sub_frame_by_type(frame, 'DataClass'),
#         sub_frame_by_type(frame, 'RefusedParentBequest')
#     )
#
#
# def sub_frame_by_type(frame, type, attribute="Lebensdauer"):
#     return frame[lifespan_frame['Typ'] == type][attribute]
#
#
# lifespan_frame['Lebensdauer in log'] = \
#     lifespan_frame.apply(lambda row: nmp.log(row.Lebensdauer), axis=1)
#
# lifespan_frame['Lebensdauer in square root'] = \
#     lifespan_frame.apply(lambda row: nmp.sqrt(row.Lebensdauer), axis=1)
#
# lifespan_frame['Lebensdauer in sqrtsqrt'] = \
#     lifespan_frame.apply(lambda row: nmp.sqrt(nmp.sqrt(row.Lebensdauer)),
#                          axis=1)
#
# lifespan_frame['Lebensdauer in sqrtsqrtsqrt'] = \
#     lifespan_frame.apply(lambda row: nmp.sqrt(nmp.sqrt(nmp.sqrt(row.Lebensdauer))),
#                          axis=1)
#
# lifespan_frame['Lebensdauer in sqrt*1.5'] = \
#     lifespan_frame.apply(lambda row: 1.5 * nmp.sqrt(row.Lebensdauer), axis=1)
#
# lifespan_frame['Lebensdauer in pow0.5'] = \
#     lifespan_frame.apply(lambda row: row.Lebensdauer ** 0.5, axis=1)
#
# lifespan_frame['Lebensdauer in powm0.5'] = \
#     lifespan_frame.apply(lambda row: row.Lebensdauer ** -0.5, axis=1)
#
# print(lifespan_frame)

# print(column_for_each_smell(lifespan_frame))
# print(stats.normaltest(sub_frame_by_type(lifespan_frame, "LongMethod", attribute='Lebensdauer in log')))
# print(stats.normaltest(sub_frame_by_type(lifespan_frame, "LongParameterList", attribute='Lebensdauer in log')))
# print(stats.normaltest(sub_frame_by_type(lifespan_frame, "FeatureEnvy", attribute='Lebensdauer in log')))
# print(stats.normaltest(sub_frame_by_type(lifespan_frame, "GodClass", attribute='Lebensdauer in log')))
# print(stats.normaltest(sub_frame_by_type(lifespan_frame, "MiddleMan", attribute='Lebensdauer in log')))
# print(stats.normaltest(sub_frame_by_type(lifespan_frame, "ShotgunSurgery", attribute='Lebensdauer in log')))
# lifespan_frame.to_csv(r'lifespan_ext2.txt',
#                       header=None, index=None, sep=' ', mode='w')


# summary = lifespan_frame.groupby('Typ').agg(
#     {'Lebensdauer': [np.mean, np.std]}) \
#     .xs('Lebensdauer', axis=1, drop_level=True)
# new = lifespan_frame.assign(z=
# lifespan_frame.join(summary, on='Typ').eval(
#     '(Lebensdauer - mean) / std'))
# bla = stats.boxcox(lifespan_frame["Lebensdauer"])
# print(stats.normaltest(new['z']))

# nda = [sub_frame_by_type(new, v, 'z') for v in type_lookup.values()]
#
# print(stats.levene(nda))
# print(bla)

# print(new)
# new.to_csv(r'lifespan_with_z.txt',
#                       header=None, index=None, sep=' ', mode='w')


# f_val, p_val = kruskal(lifespan_frame)
# print("kruskal f:", f_val)
# print("kruskal p:", p_val)

def methods_frame(frame):
    return pn.concat([
        frame.loc[lambda df: df.Typ == 'LongMethod'],
        frame.loc[lambda df: df.Typ == 'FeatureEnvy'],
        frame.loc[lambda df: df.Typ == 'ComplexMethod'],
        frame.loc[lambda df: df.Typ == 'NestedBlockDepth'],
        frame.loc[lambda df: df.Typ == 'BrainMethod'],
        frame.loc[lambda df: df.Typ == 'ComplexCondition'],
        frame.loc[lambda df: df.Typ == 'DeadCode'],
        frame.loc[lambda df: df.Typ == 'Cycle'],
        frame.loc[lambda df: df.Typ == 'LongParameterList']
    ])


def classes_frame(frame):
    return pn.concat([
        frame.loc[lambda df: df.Typ == 'GodClass'],
        frame.loc[lambda df: df.Typ == 'LargeClass'],
        frame.loc[lambda df: df.Typ == 'MiddleMan'],
        frame.loc[lambda df: df.Typ == 'ShotgunSurgery'],
        frame.loc[lambda df: df.Typ == 'ClassDataShouldBePrivate'],
        frame.loc[lambda df: df.Typ == 'TraditionBreaker'],
        frame.loc[lambda df: df.Typ == 'DataClass'],
        frame.loc[lambda df: df.Typ == 'RefusedParentBequest']
    ])


# sb.set(style="whitegrid")
# g = sb.factorplot(kind='box', x='Typ', y='Lebensdauer',
##                   g = sb.factorplot(kind='box', x='Typ', y='Relocations',
# data=lifespan_frame, legend_out=True, aspect=1.5,
# showmeans=True, size=4, orient="v", dodge=True)
# g.set_xticklabels(rotation=30)
# plt.ylim(0, 10)
# plt.show()
# sb.factorplot(kind='box', x='Typ', y='Lebensdauer', hue='Typ', data=classes_frame(lifespan_frame), legend_out=True, aspect=1.5, showmeans=True)
# # plt.savefig('5_lebensdauer_method.png', bbox_inches='tight')
# # sb.factorplot(kind='box', x='Typ', y='Lebensdauer', hue='Typ', data=methods_frame(lifespan_frame), legend_out=True, aspect=1.5, showmeans=True)
# # sb.boxplot(x='Name', y='Lebensdauer', hue='Name', data=class_lifespans)
# # plt.savefig('5_lebensdauer_class.png', bbox_inches='tight')
# # sb.despine(offset=10, trim=True)
# plt.show()

# Changes, Relocations und Revivals

changes_data = ['Typ,Modifikationen']
# changes_data = ['Typ,Wiederbelebungen']
# changes_data = ['Typ,Relocations']
type_to_changes = {}
for data in smell_type_data:
    type = data.name
    if type not in type_to_changes:
        # type_to_changes[type] = data.revivals
        type_to_changes[type] = data.changes + data.relocations
        # type_to_changes[type] = data.relocations
    else:
        current = type_to_changes[type]
        # type_to_changes[type] = current + data.revivals
        type_to_changes[type] = current + data.changes + data.relocations
        # type_to_changes[type] = current + data.relocations


# Latex table entry

class Statistics:

    def __init__(self, type, size, sum, median, mean, std, max, third, first):
        self.max = max
        self.third = third
        self.std = std
        self.first = first
        self.mean = mean
        self.median = median
        self.sum = sum
        self.size = size
        self.type = type

    def __str__(self):
        return "{} & {} & {} & {} & {} & {} & {} & {} \\\\\\hline" \
            .format(self.type, self.size, self.sum, self.median,
                    self.mean, self.third, self.max, self.std)


statistics = []
type_to_mod_percent = {}
for key, non_f_values in type_to_changes.items():
    values = list(filter(lambda elem: elem > 0, non_f_values))
    print(values)
    length = len(non_f_values)
    length_filtered = len(values)
    mod_percent = 100 / length * length_filtered
    type_to_mod_percent[key] = mod_percent
    mean = round(nmp.mean(values), 2)
    std = round(nmp.std(values), 2)
    stat = Statistics(key,
                      length_filtered,
                      nmp.sum(values),
                      nmp.median(values),
                      mean,
                      std,
                      nmp.percentile(
                          list(filter(lambda x: x < mean + 2 * std, values)),
                          100),
                      nmp.percentile(values, 75),
                      nmp.percentile(values, 25))
    statistics.append(stat)

statistics.sort(key=lambda elem: elem.mean, reverse=True)
for s in statistics:
    print(s)

percent_items = list({"{},{},{}".format(type, mod, 100 - mod) for type, mod in
                      type_to_mod_percent.items()})
percent_items.insert(0, "Typ,modifiziert,nicht modifiziert")
# percent_items.insert(0, "Typ,wiederbelebt,nicht wiederbelebt")

mod_frame = pn.read_csv(StringIO('\n'.join(percent_items))) \
    .sort_values(by='modifiziert')
# .sort_values(by='wiederbelebt')

print(mod_frame)

csv_table = ["Typ Value"]
for key, non_f_values in type_to_changes.items():
    values = list(filter(lambda elem: elem > 0, non_f_values))
    for value in values:
        csv_table.append(key + " " + str(value))

with open("modifications.txt", "w") as file:
    file.write("\n".join(csv_table))

# mod_frame[['modifiziert', 'nicht modifiziert']].plot.barh(mod_frame['Typ'], stacked=True)
# plt.savefig('5_modifiziert_nicht.png', bbox_inches='tight')

# mod_frame[['wiederbelebt', 'nicht wiederbelebt']].plot.barh(mod_frame['Typ'], stacked=True)
# plt.savefig('5_wiederbelebt_nicht.png', bbox_inches='tight')

# End Latex table entry

# for key, value in type_to_changes.items():
#     for elem in value:
#         changes_data.append(key + "," + str(elem))

# changes_frame = pn.read_csv(StringIO('\n'.join(changes_data)))
# print(changes_frame)

# g = sb.factorplot(kind='box', x='Typ', y='Modifikationen',
#                   g = sb.factorplot(kind='box', x='Typ', y='Wiederbelebungen',
#                   g = sb.factorplot(kind='box', x='Typ', y='Relocations',
# data=changes_frame, legend_out=True, aspect=1.5,
# showmeans=True, orient="v")
# g.set_xticklabels(rotation=30)
# plt.ylim(0, 10)
# plt.show()
