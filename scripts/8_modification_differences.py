from differences_as_table import *


def bla(type):
    for e in entries:
        if e.p_adj > 0.05 and (
                e.first == type or e.second == type):
            print(e)


if __name__ == '__main__':
    entries = [entry for entry in read_input("8_modification_differences.txt")]
    filtered = filter_significant_values(entries)
    print_significant_of(filtered, "ShotgunSurgery")
    # for e in filtered: print(e)
    bla("NestedBlockDepth")
