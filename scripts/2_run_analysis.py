import subprocess

reports_dir = "/home/artur/master/reports"
quide_log_file = "/home/artur/.quide/logs/platform.log"
projects_dir = "/home/artur/master/projects"
success_list = "/home/artur/master/success_list.txt"
ignore_list = "/home/artur/master/ignore_list.txt"
quide = "/home/artur/Repos/quide/quide-platform/build/libs/quide-platform-1.0.0.M3-all.jar"

pending_text = "pending"
done_text = "done"

unmappable_warning = "WARN  io.gitlab.arturbosch.quide.java.mapping.ASTPatch - ClassCastException: Recover from failing mapping"
simple_version_printout = "VersionInfo"
git_relocations_warning = "ERROR QuideGitVersionProvider - warning: inexact rename detection was skipped due to too many files"
git_relocations_warning2 = "warning: you may want to set your diff.renameLimit variable"


class LogFileNotEmpty(Exception):
    def __init__(self, message):
        super(LogFileNotEmpty, self).__init__(message)


def is_suppressed_known_warning(line):
    if not line == "":
        return unmappable_warning in line or \
               git_relocations_warning in line or \
               git_relocations_warning2 in line or \
               line.startswith(simple_version_printout)
    return True


def check_log_file_is_empty(current_project):
    try:
        with open(quide_log_file) as the_log:
            lines = the_log.readlines()
            is_ok = True
            for line in lines:
                is_ok &= is_suppressed_known_warning(line.strip())
            if not is_ok:
                raise LogFileNotEmpty(current_project)
    except FileNotFoundError:
        pass


def start_using_success_list():
    with open(success_list) as sfile:
        lines = sfile.readlines()
    with open(ignore_list) as ifile:
        ignore_lines = ifile.readlines()
        ignores = list(map(lambda elem: elem.strip(), ignore_lines))

    output = lines.copy()
    for line, content in enumerate(lines):
        if content == "":
            break
        name, is_pending = content.split("=")
        if name in ignores:
            print("Ignoring", name)
            continue
        elif is_pending.strip() == pending_text:
            print("Processing", name)
            output[line] = name + "=" + done_text + "\n"
            run_analysis_for(name)
            with open(success_list, "w") as sfile:
                sfile.writelines(output)
        else:
            print("Skipping", name)
    print(output)


def run_analysis_for(name):
    p = subprocess.Popen(('jrun', '-Xmx25000m', '-Xms10000m', quide, '-o',
                          reports_dir, '-i', projects_dir + "/" + name))
    p.wait()
    check_log_file_is_empty(name)


start_using_success_list()
print("Finished analyzing all projecs.")
