data<-read.csv("/home/artur/Repos/masterthesis/pilot/lifespan.txt", header=TRUE, sep = " ", na.strings = c("", "NA"))

# Assumption 1: All samples are independent and collected in >2 independent categories

data$Typ <- as.factor(data$Typ)

# Assumption 2: Dependent variable is continuous (Lebensdauer ist quantitativ)
# Assumption 3: normal distribution is not necessary
class(data$Typ)
Group1 <- subset(data, Typ == "LongMethod")
Group2 <- subset(data, Typ == "LongParameterList")
Group3 <- subset(data, Typ == "GodClass")
qqnorm(Group1$Value)
qqline(Group1$Value)
# ...

# Assumption 4: Homogeneneity of variances

shapiro.test(Group1$Value, Group2$Value, Group3$Value)

bartlett.test(Value ~ Typ, data = data)
install.packages("Rcmdr")
library(Rcmdr)
leveneTest(Value ~ Typ, data = data, center = median) # , center = median

#install.packages("HH")
#library(HH)
#hov(Value ~ Typ, data = data)
#hovBF(Value ~ Typ, data = data)
# When all assumptions are true run kruskal

kruskal.test(data$Typ ~ data$Value)
kruskal.test(Value ~ Typ, data = data)

install.packages("FSA")
library(FSA)

dunnTest(Value ~ Typ, data = data, method = "bonferroni")

# Data visualization

install.packages("ggplot2")
library("ggplot2")

ggplot(data, aes(x = Typ, y = Value)) + 
  geom_boxplot(fill = "grey80", colour = "black") + 
  scale_x_discrete() + xlab("Code Smell") + ylab("z")

# One way anova

model1 = lm(Value ~ Typ, data = data)
anova(model1)

oneway.test(Value ~ Typ, data = data)
TukeyHSD(aov(model1))
