install.packages("arules")
library(arules)

tr <- read.transactions("/home/artur/Repos/masterthesis/pilot/7_all_clusters.csv", format="basket", sep=' ')
inspect(tr)

r <- apriori(tr, control=list(verbose=F), parameter=list(minlen=2, supp=0.1, conf=0.5),
             appearance = list(lhs=c("NestedBlockDepth"), default="rhs"))
inspect(r)
inspect(sort(r, by="lift")[1:10])

r <- apriori(tr, control=list(verbose=F), parameter=list(minlen=2, supp=0.05, conf=0.9))
inspect(r[1:5])
inspect(sort(r, by="lift")[1:100])

summary(r)

itemFrequencyPlot(tr, topN=5)
