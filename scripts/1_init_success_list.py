import os

projects_dir = "/home/artur/master/projects"
success_list = "/home/artur/master/success_list.txt"

dirs = os.listdir(projects_dir)
entries = [current + "=" + "pending\n" for current in dirs]
entries.sort()

with open(success_list, "w") as sfile:
    for entry in entries:
        sfile.write(entry)
