import matplotlib.pyplot as plt
import os
from functools import reduce
from io import StringIO
import pandas as pn
import seaborn as sb
import numpy as nmp

with open('verteilung') as file:
    lines = [line.strip().lower() for line in file.readlines()]
    assert len(lines) == 2
    smells = lines[0].split(',')
    values = lines[1].split(',')
    int_values = [int(value.strip()) for value in values]
    sum = reduce(lambda elem, acc: acc + elem,
                 int_values)
    piece = 100.0 / sum
    percent_values = [piece * value for value in int_values]
    assert len(smells) == len(values)

    formatted = [smell + "," + str(percent_values[index])
                 for index, smell in enumerate(smells)]
    formatted.insert(0, "name,value")

    data_table = reduce(lambda elem, acc: elem + "\n" + acc, formatted)
    frame = pn.read_csv(StringIO(data_table)) \
        .reset_index() \
        .sort_values(by='value')

fig = plt.figure(figsize=(12, 12))
axes = fig.gca()
frame.plot(kind='bar', x='name', y='value', ax=axes)
axes.set_xlabel('Code Smell')
axes.set_ylabel('Vorkommen in Prozent')
plt.savefig('4_smell_distribution.png', bbox_inches='tight')
# plt.show()
