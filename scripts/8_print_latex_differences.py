from differences_as_table import *

if __name__ == '__main__':
    entries = [entry.to_latex() for entry in
               read_input("8_lifespan_differences.txt")]
    print("\n".join(entries))
    print()
    entries = [entry.to_latex() for entry in
               read_input("8_modification_differences.txt")]
    print("\n".join(entries))
