class Entry:

    def __init__(self, first, second, diff, lwr, upr, p_adj):
        self.upr = upr
        self.lwr = lwr
        self.diff = diff
        self.second = second
        self.first = first
        self.p_adj = p_adj

    def __str__(self):
        return self.group() + " " + str(self.p_adj)

    def group(self):
        return self.first + "-" + self.second

    def to_latex(self):
        return "{} & {} & {} & {} & {} \\\\\\hline" \
            .format(self.group(), round(self.diff, 2), round(self.lwr, 2),
                    round(self.upr, 2), round(self.p_adj, 2))


def read_input(path):
    with open(path) as file:
        for line in file.readlines():
            normalized = " ".join(line.split())
            tmp = normalized.split(' ')
            group = tmp[0]
            first, second = group.split('-')
            yield Entry(first, second, float(tmp[1]),
                        float(tmp[2]), float(tmp[3]), float(tmp[4]))


def filter_significant_values(entries):
    return filter(lambda x: x.p_adj < 0.05, entries)


def print_significant_of(entries, type):
    for e in entries:
        if e.first == type or e.second == type:
            print(e)
