import os
from functools import reduce
from io import StringIO

import pandas as pn
import matplotlib.pyplot as plt
from pylab import *
import seaborn as sb
import numpy as nmp
import string
from itertools import groupby
from scipy import stats
import dunn_test

frame = pn.read_csv('newfile2.txt', sep=' ')
print(frame)


def sub_frame_by_type(frame, type, attribute="LogValue"):
    return frame[frame['Typ'] == type][attribute]


# fig = plt.figure(figsize=(12, 12))
# axes = fig.gca()
# frame.plot.bar(ax=axes)
# axes.set_xlabel('Projekt')
# axes.set_ylabel('Anzahl Commits')

frame.hist()
# frame['LogValue'].plot(kind='bar')
# print(stats.normaltest(sub_frame_by_type(frame, 'LongMethod')))
# print(stats.normaltest(sub_frame_by_type(frame, 'LongParameterList')))

plt.show()
